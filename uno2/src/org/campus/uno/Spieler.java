package org.campus.uno;

import java.util.ArrayList;

public class Spieler
{
	private String name;
	private ArrayList<Karte> handkarten= new ArrayList<Karte>();
	
	//constructor um ein Spieler zu erstellen
	public Spieler(String name)
	{
		this.name=name;
	}
	//ein spieler kann eine Karte aufnehemen
	public void aufnehmen(Karte karte)
	{
		handkarten.add(karte);
	}
	public String toString()
	{
		return name + " " +handkarten;
	}
	
	// vergleichenn 2 karten  
	public Karte passendeKarte(Karte vergleich)
	{
		
		for(Karte karte: handkarten)
		{
			if(karte.match(vergleich))
			{
				handkarten.remove(karte);
				if(handkarten.size()==1)
				{
					System.out.println("UNO");
				}
				return karte;
			}
		}
		return null;
	}
	
	public int anzahlDerHandKarten()
	{
		return handkarten.size();
	}

}
