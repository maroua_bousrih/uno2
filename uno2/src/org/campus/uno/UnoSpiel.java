package org.campus.uno;

import java.util.ArrayList;
import java.util.Collections;

public class UnoSpiel
{
	private ArrayList<Karte> ablageStapel = new ArrayList<Karte>();
	private ArrayList<Karte> kartenStapel = new ArrayList<Karte>();
	private ArrayList<Spieler>mitspieler = new ArrayList<Spieler>();
	
	 //konstruktor  we creat all cart for the game
	public UnoSpiel()
	{
		for(int index=0; index<2;index++)
		{
		for(int zahlenwert=0; zahlenwert<10; zahlenwert++ )
		{
			kartenStapel.add(new Karte(Farbe.rot, zahlenwert)); //creation des cartes rouges de 0 a 9
			kartenStapel.add(new Karte(Farbe.blau, zahlenwert));
			kartenStapel.add(new Karte(Farbe.gruen, zahlenwert));
			kartenStapel.add(new Karte(Farbe.gelb, zahlenwert));

			
		}
		}
	
	 Collections.shuffle(kartenStapel); //  karten mischen
	
	}
	public Karte abheben()
	{	// sind noch genung karten am stapel?
		if(kartenStapel.size()==0)
		{
			//obersre karte merken
			Karte obersteKarte= ablageStapel.remove(0);
			Collections.shuffle(ablageStapel);//restlichen stapel mischen
			//aus ablagestapel neuen kartenstapel machen 
			kartenStapel.addAll(ablageStapel); //alle karten einf�gen
			ablageStapel.clear();// kartenstapel leeren ind
			ablageStapel.add(obersteKarte); // oberste karte wieder drauf legen
					
			
		}
		return kartenStapel.remove(0);
	}
	//die karte die ich m�chte ablegen
	public void KarteAblegen(Karte ablegeKarte)//liefert keine Karte zur�ck
	{
		ablageStapel.add(0,ablegeKarte);
	}
  
	
	//methode neue Spiler
	public void mitSpielen(Spieler neuerSpieler)
	{
		mitspieler.add(neuerSpieler);
		
	}
	
	public boolean spielzug()
	{
		
		// wer ist dran
		Spieler aktuellerSpieler= mitspieler.remove(0);
		System.out.println("am zug ist: "+ aktuellerSpieler);
		//spieler vergleich ob die oberste karte am ablagestapel mit dem handkarten
		Karte gefundeneKarte = aktuellerSpieler.passendeKarte(ablageStapel.get(0));
		if (gefundeneKarte != null)
		{
			System.out.println("karte ablegen: " + gefundeneKarte);
		 KarteAblegen(gefundeneKarte);	
		 
		}
		else
		{
			System.out.println("karte ziehen");
			Karte abgehobeneKarte= abheben();
			if (abgehobeneKarte.match(ablageStapel.get(0)))
			{
				System.out.println(" ablegen : "+ abgehobeneKarte);
				KarteAblegen(abgehobeneKarte);
			}
			else
			{
				System.out.println("karte aufnehmen");
				aktuellerSpieler.aufnehmen(abgehobeneKarte);
			}
		}
		if(aktuellerSpieler.anzahlDerHandKarten()==0)
		{
			System.out.println("gewonnen hat: " + aktuellerSpieler);
			return false;
		}
		
		//spielzug ende spieler objekt als letztes in die liste 
		mitspieler.add(aktuellerSpieler);
		return true;
	}
	public void austeilen()
	{ 
		//gib jeder spiler 7 karte
		for (int zaehler =0 ; zaehler < 7 ; zaehler ++)
		{
		//jeder Mitspieler einmal besuchen
		for (Spieler sp : mitspieler)
		{
			Karte karte =abheben();
			sp.aufnehmen(karte);
		}
		}
		
		Karte temp =abheben();
		ablageStapel.add(temp);
		System.out.println(temp);
		
	}
  
 
}
