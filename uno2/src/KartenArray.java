import java.util.ArrayList;

import org.campus.uno.Farbe;
import org.campus.uno.Karte;


public class KartenArray
{

	public static void main(String[] args)
	{
		
		Karte[] meineKarten = new Karte[]
		{ new Karte(Farbe.rot,1),  new Karte (Farbe.blau, 1), new Karte (Farbe.gelb, 1)};
		//nur index 1
		
		System.out.println("die Inhalt von die zweite object" +  meineKarten[1]);
		
//		for (Karte karte: meineKarten)
//		{
//			System.out.println(karte);
//		}
		//type ArrayList die karten verwalten kann
		ArrayList<Karte> meineListe = new ArrayList<Karte>();
		//eien karte in die Liste einf�gen
		meineListe.add(new Karte(Farbe.rot,2));
		
		
		
		meineListe.add(new Karte(Farbe.blau,3));
		meineListe.add(new Karte(Farbe.gelb,5));
		
		// l�nge von liste
		System.out.println(meineListe.size());
		
		// gibt mir die karte an der position 0
		System.out.println(meineListe.get(1));
		

		
		//Inhalt der List
		System.out.println("Inhalt der List");
		for(Karte karte : meineListe)
		{
			
			System.out.println( karte);
		}
		
		System.out.println(meineListe.remove(0) +" will be remove");
		
		System.out.println(meineListe.size());
		
		System.out.println(meineListe);
		
		

	}

}
